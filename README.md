# JoeBot

Before running: 

Setup a bot at https://discordapp.com/developers/applications
Get the token under the 'Bot' tab and place into joebot.py for variable 'TOKEN'

run (required pip installed):
python -m pip install discord.py==0.16.12

If using Python 3.7:
Replace all of the below references in all discord Python files downloaded from pip (I think it's in 3 or 4 files)

from:
asyncio.async

to:
getattr(asyncio, 'async')
